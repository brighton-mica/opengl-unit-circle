#include <iostream>
#include <vector>
#include <string>

#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "IMGUI/imgui.h"
#include "IMGUI/imgui_impl_glfw.h"
#include "IMGUI/imgui_impl_opengl3.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "classes/Window.h"
#include "classes/Mesh.h"
#include "classes/Shader.h"

const GLint WINDOW_WIDTH = 800;
const GLint WINDOW_HEIGHT = 600;
Window window;
unsigned int Mesh::meshId{ 0 };
std::vector<Mesh*> meshList;
std::vector<Shader*> shaderList;
const char* vShader = "src/shaders/shader.vert";
const char* fShader = "src/shaders/shader.frag";
const float toRadians = 3.14159265f / 180.0f;

void CreateObjects() 
{
	// Circle
	GLfloat circleVertices[1080] = { 0 };

	for (unsigned int i = 0; i < 360; i += 1) 
	{
		circleVertices[i * 3] = glm::cos(toRadians * i);
		circleVertices[i * 3 + 1] = glm::sin(toRadians * i);
		circleVertices[i * 3 + 2] = 0.0f;
	}

	unsigned int circleIndices[360] = { 0 };
	for (unsigned int i = 0; i < 359; i++) { circleIndices[i] = i; }

	Mesh* obj = new Mesh();
	obj->createMesh(circleVertices, circleIndices, 1080, 360, GL_LINE_LOOP);
	meshList.push_back(obj);

	// Axis
	GLfloat axisVertices[15] = {
		1.0f, 0.0f, 0.0f,   // right
		0.0f, 1.0f, 0.0f,   // top
		-1.0f, 0.0f, 0.0f,  // left
		0.0f, -1.0f, 0.0f,  // bottom
		0.0f, 0.0f, 0.0f    // middle
	};

	unsigned int axisIndices[] = { 0, 2, 4, 1, 3 };

	obj = new Mesh();
	obj->createMesh(axisVertices, axisIndices, 15, 5, GL_LINE_STRIP);
	meshList.push_back(obj);

	// Hypotenuse
	GLfloat hypotVertices[6] = {
		0.0f, 0.0f, 0.001f,
		1.0f, 0.0f, 0.001f
	};

	unsigned int hypotIndices[2] = { 0, 1 };

	obj = new Mesh();
	obj->createMesh(hypotVertices, hypotIndices, 6, 2, GL_LINE_STRIP);
	meshList.push_back(obj);

	// X
	GLfloat xVertices[6] = {
		0.0f, 0.0f, 0.001f,
		1.0f, 0.0f, 0.001f
	};

	unsigned int xIndices[2] = { 0, 1 };

	obj = new Mesh();
	obj->createMesh(xVertices, xIndices, 6, 2, GL_LINE_STRIP);
	meshList.push_back(obj);

	// Y
	GLfloat yVertices[6] = {
		0.0f, 0.0f, 0.001f,
		0.0f, 1.0f, 0.001f
	};

	unsigned int yIndices[2] = { 0, 1 };

	obj = new Mesh();
	obj->createMesh(yVertices, yIndices, 6, 2, GL_LINE_STRIP);
	meshList.push_back(obj);

	// Triangle Mesh 
	GLfloat triVertices[9] = {
		0.0f, 0.0f, 0.0f, // middle
		1.0f, 1.0f, 0.0f, // y
		1.0f, 0.0f, 0.0f  // x
	};

	unsigned int triIndices[3] = { 1, 0, 2 };

	obj = new Mesh();
	obj->createMesh(triVertices, triIndices, 9, 3, GL_TRIANGLES);
	meshList.push_back(obj);
}

void CreateShaders()
{
	Shader* shader1 = new Shader();
	shader1->createFromFiles(vShader, fShader);
	shaderList.push_back(shader1);
}

int main() {
	window = Window(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (!window.init()) { return 1; }

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	// ImGui color
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(window.getWindow(), true);
	ImGui_ImplOpenGL3_Init((char*)glGetString(GL_NUM_SHADING_LANGUAGE_VERSIONS));

	CreateObjects();
	CreateShaders();

	GLuint uniformProjection = 0, uniformModel = 0, uniformColor = 0;
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (GLfloat)window.getBufferWidth() / window.getBufferHeight(), 0.1f, 100.0f);

	float currAngle = 0.0f;
	bool show_demo_window = true;
	bool show_another_window = false;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	float theta = 0.0;

	while (!window.shouldClose()) {

		// Get + handle user input
		glfwPollEvents();

		// Start the Dear ImGui frame
		 ImGui_ImplOpenGL3_NewFrame();
		 ImGui_ImplGlfw_NewFrame();
		 ImGui::NewFrame();

		 //if (theta > 360) { theta -= 360; }
		 //theta += 1;

		glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderList[0]->useShader();
		{
			uniformProjection = shaderList[0]->getProjectionLocation();
			uniformModel = shaderList[0]->getModelLocation();
			uniformColor = shaderList[0]->getColorLocation();
			glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
			
			// Circle, Axis
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.0f));
			model = glm::scale(model, glm::vec3(0.6f, 0.6f, 1.0f));
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			glUniform3f(uniformColor, 0.0f, 0.0f, 0.0f);
			meshList[0]->renderMesh();
			meshList[1]->renderMesh();

			// Hypotenuse
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.0f));
			model = glm::rotate(model, theta * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
			model = glm::scale(model, glm::vec3(0.6f, 0.6f, 1.0f));
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			glUniform3f(uniformColor, 0.19f, 0.6f, 0.17f);
			meshList[2]->renderMesh();

			// X
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.0f));
			model = glm::scale(model, glm::vec3(glm::cos(theta * toRadians) * 0.6f, 0.6f, 1.0f));
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			meshList[3]->renderMesh();

			// Y
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(0.6f * glm::cos(theta * toRadians), 0.0f, -2.0f));
			model = glm::scale(model, glm::vec3(1.0f, glm::sin(theta * toRadians) * 0.6f, 1.0f));
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			meshList[4]->renderMesh();

			// Triangle
			model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.0f));
			model = glm::scale(model, glm::vec3(glm::cos(theta * toRadians) * 0.6f, glm::sin(theta * toRadians) * 0.6f, 1.0f));
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			glUniform3f(uniformColor, 0.56f, 1.0f, 0.36f);
			meshList[5]->renderMesh();
		}
		glUseProgram(0);


		{
			static float f = 0.0f;
			static int counter = 0;

			ImGui::Begin("Unit Circle");                          // Create a window called "Hello, world!" and append into it.
			ImGui::SliderFloat("Theta", &theta, 0.0f, 360.0f);
			std::string s = "Sin: ", c = "Cos: ";
			ImGui::Text((s + std::to_string(glm::sin(theta * toRadians))).c_str());
			ImGui::Text((c + std::to_string(glm::cos(theta * toRadians))).c_str());

			//ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
			//ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
			//ImGui::Checkbox("Another Window", &show_another_window);

			//ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
			//ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

			//if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
			//	counter++;
			//ImGui::SameLine();
			//ImGui::Text("counter = %d", counter);

			//ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::End();
		}

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		window.swapBuffers();
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	for (int i = 0; i < meshList.size(); i++) 
		delete meshList[i];

	for (int i = 0; i < shaderList.size(); i++)
		delete shaderList[i];
}