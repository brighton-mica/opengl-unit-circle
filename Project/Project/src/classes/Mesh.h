#pragma once
#include <GL\glew.h>
#include <iostream>

class Mesh {
public:
	Mesh();
	~Mesh();

	void createMesh(GLfloat* vertices, unsigned int* indices, unsigned int numOfVertices, unsigned int numOfIndices, GLenum primitive);
	void renderMesh();
	void clearMesh();

private:
	GLuint VAO, VBO, IBO;
	GLsizei indexCount;
	GLenum glPrimitive;
	static unsigned int meshId;
	unsigned int id;
};

